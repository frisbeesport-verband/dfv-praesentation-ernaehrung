# Beamer: Ernährung im Sport
Sehr grundlegende Präsentation für Frisbeesportler über Ernährung.

 * Vorlage: [DFV Beamer Theme](https://gitlab.com/PlusMinus0/dfv-beamer-template)
 * [Download](https://gitlab.com/PlusMinus0/dfv-praesentation-ernaehrung/-/jobs/artifacts/master/download?job=build)
 * [Vorschau](https://gitlab.com/PlusMinus0/dfv-praesentation-ernaehrung/-/jobs/artifacts/master/file/beamer.pdf?job=build)

